

package com.mycompany.empleado;
import java.text.ParseException;
import java.text.SimpleDateFormat ; 
import java.util.Date; 
public class Empleado {

   private String nombre;
   private int salario;
   private Date fechadecontratacion;
   

public Empleado (String nombre, int salario, Date fechadecontratacion ) {
        this.nombre = nombre;
        this.salario = salario;
        this.fechadecontratacion= fechadecontratacion;
        
    }

    Empleado() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public String getnombre() {
        return this.nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }

    public int getsalario() {
        return salario;
    }

    public void setsalario(int salario) {
        this.salario = salario;
    }

    public String getfechadecontratacion() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("DD/MM/AAA");
        return dateFormat.format (fechadecontratacion);
    }

    public void setfechadecontratacion( String  fechadecontratacion) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("DD/MM/AAA");
        this.fechadecontratacion = dateFormat.parse(fechadecontratacion);
    }

    public double calculateBonus (double porcentaje ){
        
        return(porcentaje /100)*salario;
    }   


}