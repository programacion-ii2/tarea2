/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.accountholder;

/**
 *
 * @author USUARIO
 */
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese su nombre: ");
        String name = scanner.nextLine();
        System.out.print("Ingrese su número de cédula: ");
        String ci = scanner.nextLine();
        System.out.print("Ingrese su dirección: ");
        String address = scanner.nextLine();
        System.out.print("Ingrese su número de teléfono: ");
        String phoneNumber = scanner.nextLine();
        System.out.print("Ingrese su dirección de correo electrónico: ");
        String email = scanner.nextLine();

        AccountHolder accountHolder = new AccountHolder(name, ci, address, phoneNumber, email);
        BankAccount bankAccount = new BankAccount(1000.0, accountHolder);

        while (true) {
            System.out.println("Menú:");
            System.out.println("1. Crear cuenta");
            System.out.println("2. Depositar dinero");
            System.out.println("3. Retirar dinero");
            System.out.println("4. Comprobar saldo");
            System.out.println("5. Salir");

            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("Cuenta creada ");
                    break;
                case 2:
                    System.out.print("Ingrese la cantidad a depositar: ");
                    double deposito = scanner.nextDouble();
                    bankAccount.deposit(deposito);
                    System.out.println("Depósito exitoso.");
                    break;
                case 3:
                    System.out.print("Ingrese la cantidad a retirar: ");
                    double retiro = scanner.nextDouble();
                    String mensajeRetiro = bankAccount.withdraw(retiro);
                    System.out.println(mensajeRetiro);
                    break;
                case 4:
                    System.out.println(bankAccount.queryBalance());
                    break;
                case 5:
                    System.out.println("¡Hasta luego!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opción no válida");
            }
        }
    }
}

    

