/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.student;

/**
 *
 * @author USUARIO
 */

import java.util.Scanner; 
public class main {
    public static void main (String[]args){
    
    StudentsList studentsList = new StudentsList (); 
    Scanner in = new Scanner( System.in);
    int opcion ; 
    do{
       System.out.println("a continuacion escoja una de las opcion "); 
       System.out.println(" 1. añardir a un nuevo estudiante en la lista"); 
       System.out.println(" 2. mostrar la lista de estudiantes ");
       System.out.println(" 3. buscar un estudiante por su identificacion");
       System.out.println("salir");
       
       opcion = in.nextInt(); 
       switch (opcion ){
           case 1: 
               System.out.println("añadir un nuevo estudiante en la lista");
               System.out.println("ingrese el nombre del estudiante ");
               String name= in.nextLine(); 
               System.out.println("ingrese la edad del estudiante ");
               int edad = in.nextInt(); 
               System.out.println(" ingrese la identificacion del estudiante");
               String id = in.nextLine(); 
               studentsList.addStudent(name,edad,id); 
               break; 
           case 2: 
               System.out.println(" mostrar la lista de estudiantes ");
               StudentsList.showStudents (); 
               break ; 
               
           case 3 : 
               System.out.println(" busque al estudiante por su identificacion ");
               System.out.println (" ingrese su identificacion "); 
               String searchId= in.nextLine(); 
               Student foundStudent=studentsList.buscarStudentById(searchId); 
               if(foundStudent != null){
                   System.out.println("el estudiante fue encontrado");
                   foundStudent.showInformation();
               
               }else {
                   System.out.println(" el estudiante no fie encontrado ");
               }
                break ;        
           case 4 : 
               
               System.out.println("hasta luego");
               break ; 
               
               
           default : 
               System.out.println(" error");
       }
    }while (opcion != 4); 
    
  }   
}
